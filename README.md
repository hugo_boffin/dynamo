# README #

### What is this repository for? ###

Dynamo is a small tool that detects an IP mismatch between a specific DNS record and your external IP. 


### How do I get set up? ###

Code is written in Java. Maven is used as build system.

You have to fill in all fields in config.xml.sample and rename it to config.xml.

This project uses dnsjava and Apache commons-email.


### Planned featuers ###

At the moment, Dynamo only sends an email. I will refactor this to allow more events, like automatically updating the DNS record.