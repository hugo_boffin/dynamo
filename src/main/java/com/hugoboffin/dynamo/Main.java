package com.hugoboffin.dynamo;

/**
 * Created by christian on 23.06.2014.
 */
public class Main {
    public static void main(String[] args) {
        final IPChecker IPChecker = new IPChecker(Configuration.get());
        final Notifier notifier = buildNotifier(Configuration.get());

        while (true) {
            System.out.println("Checking IP...");
            try {
                final String externalIP = IPChecker.getExternalIP();
                final String dnsLookupIP = IPChecker.getDNSLookupIP();
                if (!externalIP.equals(dnsLookupIP)) {
                    notifier.notifyChanged(externalIP, dnsLookupIP);
                } else
                    notifier.notifyUnchanged(externalIP);
            } catch (Exception e) {
                notifier.notifyError(e);
            }

            if (args.length> 0 && args[0].equals("singleRun"))
                System.exit(0);

            System.out.println("Waiting for 3 hours...");
            try {
                Thread.sleep(1000 * 60 * 60 * 3);
            } catch (InterruptedException e) {
                notifier.notifyError(e);
            }
        }
    }

    private static Notifier buildNotifier(Configuration configuration) {
        switch (configuration.getNotifierType()) {
            case MAIL:
                return new MailNotifier(configuration);
            case CONSOLE:
                return new ConsoleNotifier();
        }
        return null;
    }

}