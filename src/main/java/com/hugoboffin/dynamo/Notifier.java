package com.hugoboffin.dynamo;

/**
 * Created by christian on 11.05.2016.
 */
public interface Notifier {
    void notifyChanged(String realIP, String recordIP);
    void notifyUnchanged(String ip);
    void notifyError(Exception e);
}
