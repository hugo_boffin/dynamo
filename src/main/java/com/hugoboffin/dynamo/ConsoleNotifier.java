package com.hugoboffin.dynamo;

/**
 * Created by christian on 11.05.2016.
 */
public class ConsoleNotifier implements Notifier {
    @Override
    public void notifyChanged(String realIP, String recordIP) {
        System.out.println("IP CHANGED! REAL " + realIP + " <-> DNS " + recordIP );
    }

    @Override
    public void notifyUnchanged(String ip) {
        System.out.println("OK! IP is " + ip);
    }

    @Override
    public void notifyError(Exception e) {
        System.out.println("ERROR!\n\n" + e.toString());
    }
}
