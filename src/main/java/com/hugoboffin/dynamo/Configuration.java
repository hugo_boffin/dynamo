package com.hugoboffin.dynamo;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;

/**
 * Created by christian on 6/25/2014.
 */
@XmlRootElement
public class Configuration {
    private static final File configFile = new File("config.xml");
    private static Configuration configuration = null;
    @XmlElement
    private String domain;
    @XmlElement
    private String[] externalIPTester;
    @XmlElement
    private String email;
    @XmlElement
    private String smtpAddr;
    @XmlElement
    private int smtpPort;
    @XmlElement
    private String user;
    @XmlElement
    private String pass;

    public NotifierType getNotifierType() {
        return notifierType;
    }

    @XmlElement
    private NotifierType notifierType;

    public static Configuration get() {
        if (configuration == null) {
            try {
                JAXBContext context = JAXBContext.newInstance(Configuration.class);
                Unmarshaller um = context.createUnmarshaller();
                configuration = (Configuration) um.unmarshal(configFile);
            } catch (JAXBException e) {
                e.printStackTrace();
                System.out.println("Unable to load config file!");
            }
        }

        return configuration;
    }

    public String getDomain() {
        return domain;
    }

    public String[] getExternalIPTester() {
        return externalIPTester;
    }

    public String getEmail() {
        return email;
    }

    public String getSmtpAddr() {
        return smtpAddr;
    }

    public int getSmtpPort() {
        return smtpPort;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }
}
