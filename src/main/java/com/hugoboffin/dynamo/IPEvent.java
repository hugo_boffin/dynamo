package com.hugoboffin.dynamo;

/**
 * Created by christian on 11.05.2016.
 */
public enum IPEvent {
    CHANGED, UNCHANGED, ERROR
}
