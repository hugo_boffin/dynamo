package com.hugoboffin.dynamo;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;

import java.util.Date;

/**
 * Created by christian on 11.05.2016.
 */
public class MailNotifier implements Notifier {
    private final Configuration configuration;

    public MailNotifier(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void notifyChanged(String realIP, String recordIP) {
        System.out.println("REAL " + realIP + " <-> DNS " + recordIP);
        sendMail(String.format("IP has changed!\nUpdate DNS records!!!\n\nOld IP: %s\n New IP: %s", recordIP, realIP));
    }

    @Override
    public void notifyUnchanged(String ip) {
        System.out.println("Everything looks good!");
    }

    @Override
    public void notifyError(Exception e) {
        sendMail(e.toString());
    }

    private void sendMail(String msg) {
        try {
            Email email = new SimpleEmail();
            email.setHostName(configuration.getSmtpAddr());
            email.setSmtpPort(configuration.getSmtpPort());
            email.setAuthenticator(new DefaultAuthenticator(configuration.getUser(), configuration.getPass()));
            email.setStartTLSRequired(true);
            email.setFrom(configuration.getEmail());
            email.addTo(configuration.getEmail());

            email.setSubject("Dynamo - Status mail (" + new Date().toString() + ")");
            email.setMsg(msg);

            email.send();
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("Unable to send mail. Have a nice day!");
        }
    }
}
