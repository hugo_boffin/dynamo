package com.hugoboffin.dynamo;

/**
 * Created by christian on 12.05.2016.
 */
public enum NotifierType {
        MAIL, CONSOLE
}
