package com.hugoboffin.dynamo;

import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by christian on 25.06.2014.
 */
public class IPChecker {

    private final Configuration configuration;

    private int extIPidx = 0;

    public IPChecker(Configuration configuration) {
        this.configuration = configuration;
    }

    public String getExternalIP() throws Exception {
        return getExternalIPrec(0);
    }

    private String getExternalIPrec(int depth) throws Exception {
        if (depth >= configuration.getExternalIPTester().length)
            throw new Exception("No IP check website available!");

        try {
            final URL url = new URL(configuration.getExternalIPTester()[extIPidx++ % configuration.getExternalIPTester().length]);
            final BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            final String ip = in.readLine();
            return ip;
        } catch (Exception e) {
            System.out.println(e.toString());
            return getExternalIPrec(depth+1);
        }
    }

    public String getDNSLookupIP() throws Exception {
        Lookup lookup = new Lookup(configuration.getDomain());
        final Record[] run = lookup.run();
        return run[0].rdataToString();
    }
}
